package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestConvert(t *testing.T) {
	var tests = []struct {
		name    string
		input   string
		expTime string
		expErr  error
	}{
		{"#1", "12:01:00PM", "12:01:00", nil},
		{"#2", "12:01:00AM", "00:01:00", nil},
		{"#3", " ", "", &time.ParseError{Layout: "15:04:05", Value: " ", LayoutElem: "15", ValueElem: " ", Message: ""}},
		{"#4", "07:45:12", "07:45:12AM", nil},
		{"#5", "20:00:59", "08:00:59PM", nil},
		{"#6", "12:24:14AM", "00:24:14", nil},
		{"#7", "agadsdfjhf", "", &time.ParseError{Layout: "03:04:05PM", Value: "agadsdfjhf", LayoutElem: "03", ValueElem: "agadsdfjhf", Message: ""}},
		{"#8", "05:57:42PM", "17:57:42", nil},
		{"#9", "", "", &time.ParseError{Layout: "15:04:05", Value: "", LayoutElem: "15", ValueElem: "", Message: ""}},
		{"#10", "16:59:59PM", "", &time.ParseError{Layout: "03:04:05PM", Value: "16:59:59PM", LayoutElem: "03", ValueElem: ":59:59PM", Message: ": hour out of range"}},
	}
	for _, e := range tests {
		t.Run(e.name, func(t *testing.T) {
			t.Parallel()
			convertedTime, err := convertTime(e.input)

			assert.Equal(t, e.expTime, convertedTime)
			assert.Equal(t, e.expErr, err)
		})
	}
}
