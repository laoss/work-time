package main

import (
	"fmt"
	"log"
	"time"
)

func main() {
	s := ""

	t, err := convertTime(s)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(t)
}

// convertTime converts time to opposite format
func convertTime(str string) (string, error) {
	if len(str) > 8 {
		t, err := time.Parse("03:04:05PM", str)
		if err != nil {
			return "", err
		}

		return t.Format("15:04:05"), nil
	}
	t, err := time.Parse("15:04:05", str)
	if err != nil {
		return "", err
	}

	return t.Format("03:04:05PM"), nil
}
